import React, { useState, createContext } from 'react';

export const MovieContext = createContext('deafult value');

export const MovieProvider = props => {
    const [movies, setMovies] = useState([
        {
            name: 'Bladerunner',
            price: '$10',
            id: 23124
        },
        {
            name: 'Bladerunner 2049',
            price: '$10',
            id: 2566124
        },
        {
            name: 'Her',
            price: '$10',
            id: 23524
        }
    ]);

    const m = 1;
    return (
        <MovieContext.Provider value={'1'}>
            {props.children}
        </MovieContext.Provider>
    );
};