import React, {useContext} from 'react';
import Movie from './Movie';
import { MovieContext } from './MovieContext';

const MovieList = () => {
    console.log(useContext(MovieContext));
    const c = useContext(MovieContext);
    console.log(c);
    // const [movies, setMovies] = useContext(MovieContext);

    return(
        <div>
            <h1>Value: {c}</h1>
            {/* {movies.map(movie => (
                <Movie name={movie.name} price={movie.price} key={movie.id}/>
            ))} */}
        </div>
    );
};

export default MovieList;